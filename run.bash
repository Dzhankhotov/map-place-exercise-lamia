#!/bin/bash
arg1=$1

db_folder=~/databases/map-place-exercise

show_help() {
    echo 'Without arguments runs in Docker'
    echo '--build: rebuild with cache and run in Docker'
    echo '--rebuild: run in Docker with full rebuild'
}

create_django_su() {
    echo -e -n 'Do you want to add superuser? [ Y/N ]: '
    read -t 3 -n 1 -r answer
    echo -e '\n\n'

    answer=$(echo $answer | awk '{print toupper($0)}')

    if [ "$answer" = "Y" ]; then
        docker-compose run mpe python3 mpe_project/manage.py createsuperuser
    fi
}

auto_migrate() {
    docker-compose run mpe python3 mpe_project/manage.py makemigrations mpe_app
    docker-compose run mpe python3 mpe_project/manage.py migrate
}

# Run in Docker without build
run_docker() {
    auto_migrate
    create_django_su
    docker-compose up
}

# Build installing missing stuff only
run_docker_build() {
    remove_database
    docker-compose up -d --build
    echo -e '\n\nRESTARTING CONTAINERS...\n\n'
    docker-compose stop
    run_docker
}

# Full rebuild downloading all requirements
run_docker_rebuild() {
    remove_database
    docker build --no-cache .
    run_docker
}

remove_database() {

    echo -e -n 'Do you want to remove database? [ Y/N ]: '
    read -t 5 -n 1 -r answer
    echo -e '\n\n'

    answer=$(echo $answer | awk '{print toupper($0)}')

    if [ "$answer" = "Y" ]; then
        #Checiking if already sudo
        sudo -n true 2> /dev/null

        if [ "$?" -ne "0" ]; then
            echo -e 'Please provide sudo rights\n'
        fi

        sudo rm -R $db_folder

        if [ "$?" -eq "0" ]; then
            echo -e 'Database has been successfully removed\n'
        fi
    fi
}

check_docker() {

    echo -e '\nChecking if user is in docker group...'

    if groups $USER | grep -o 'docker' > /dev/null; then
        echo -e '\nUser is already in docker group. Continuing...\n'
    else
        echo -e '\n================================================================='
        echo 'WARNING! User is not in docker group'
        echo 'Adding user to group docker trying to fix other possible issues'
        echo -e '=================================================================\n'

        echo -e '\nInstalling required stuff. Please allow sudo.\n'
        sudo apt install docker docker.io docker-compose

        echo -e '\nAdding user in docker group.\n'
        sudo usermod -aG docker $USER

        echo -e '\nIN UBUNTU-BASED DISTROS, please remember to lmpe out and lmpe in!!!\n'

        # I decided to interpret this situation as error
        # as target (to run Django server successfully) is not reached
        exit 1
    fi
}

# Routine
if [ -z $arg1 ]; then
    check_docker
    run_docker
else
    if [ "$arg1" == "--build" ]; then
        check_docker
        run_docker_build
    elif [ "$arg1" == "--rebuild" ]; then
        check_docker
        run_docker_rebuild
    else
        show_help
    fi
fi