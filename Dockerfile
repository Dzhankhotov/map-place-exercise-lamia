FROM phusion/baseimage

RUN apt-get update
RUN apt-get install -y \
    python3 python3-pip libmysqlclient-dev \
    # MariaDB stuff
    libmariadb-client-lgpl-dev libssl-dev \
    python3-setuptools

ENV PYTHONUNBUFFERED 1
RUN mkdir /map-place-exercise
WORKDIR /map-place-exercise
ADD requirements.txt /map-place-exercise/
RUN pip3 install -r requirements.txt
ADD . /map-place-exercise/

# Use /bin/bash as entrypoint
ENTRYPOINT ["/sbin/my_init"]

# Run startup script as default command
CMD ["/map-place-exercise/start.sh"]
