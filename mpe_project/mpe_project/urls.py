"""mpe_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include, handler400, handler403, handler404, handler500
from django.contrib import admin
from mpe_app import views

urlpatterns = [
    url(r'^$', views.map_place, name='map-place'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/places/', include('mpe_app.api.urls', namespace='api-places')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += [
        url(r'^400/$', views.error400),
        url(r'^403/$', views.error403),
        url(r'^404/$', views.error404),
        url(r'^500/$', views.error500),
    ]

handler400 = views.error400
handler403 = views.error403
handler404 = views.error404
handler500 = views.error500
