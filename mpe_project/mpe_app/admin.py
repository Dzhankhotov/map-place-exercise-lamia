from django.contrib import admin
from django.http import HttpResponse
from importlib import import_module
from .models import Place

class MpeAdmin(admin.ModelAdmin):
    list_display = ('title', 'id')
    search_fields = ('title', 'description')
    ordering = ['title']

admin.site.register(Place, MpeAdmin)

