// Main constructor of the app
class MpeApp {
    constructor() {
        this.API_PREFIX = window.location.href + 'api/places/';
        this.inputs = [
            'title',
            'description',
            'longitude',
            'latitude',
            'keywords',
            'opening_hour',
            'closing_hour',
            'associated_places'
        ];
        // Needed for markers deletion
        this.markers = [];
    }

    // Gets pseudo form data
    get_inputs() {

        let values = {};

        for (let i = 0; i < this.inputs.length; i++) {
            values[this.inputs[i]] = document.getElementById(this.inputs[i]).value;
        };

        return values;
    };

    // Fills inputs of pseudo form
    set_inputs(data, callback) {

        let self = this;

        for (let i = 0; i < this.inputs.length; i++) {
            // I'm just having fun here displaying fields one by one
            // This effect also makes value updates more noticeable.
            setTimeout(() => {
                document.getElementById(self.inputs[i]).value = (
                    data === 'clear' ? '' : data[self.inputs[i]]
                );
                if (callback) callback();
            }, i * 80);
        };

    };

    // Ensuring that title is available in form
    empty_title() {
        if (document.getElementById('title').value !== '') {
            return false;
        } else {
            // In real app I'd put it in a message box on page - no time for that now
            alert('Please input place title');
            return true;
        };
    };

    // This method unifies XMLHttpRequests for different HTTP methods
    // Promisifying seem to be reasonable as we use combinations of async requests
    xhr(method, url, success_status, data) {

        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();

            xhr.open(method, url, true);

            if (method === 'POST' || method === 'PUT') {
                xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            };

            xhr.onload = () => {

                if (xhr.readyState == 4 && xhr.status == success_status) {
                    if (method !== 'DELETE') {
                        this.data = JSON.parse(xhr.responseText);

                        switch(method) {
                            case 'GET':
                                if (this.data[0] === undefined) {
                                    alert('Nothing found in database.');
                                    this.data.push({});
                                    for (let i = 0; i < this.inputs.length; i++) {
                                        this.data[0][this.inputs[i]] = '';
                                    };
                                    this.pk = -1;
                                } else {
                                    this.pk = this.data[0].pk;
                                };
                                break;
                            case 'POST':
                                alert('New item is saved in database');
                                break;
                            case 'PUT':
                                alert('Item has been updated');
                                break;
                        };
                    } else {
                        alert('Item has been successfully removed from database');
                    };
                    resolve(xhr.response);
                } else {
                    switch (method) {
                        case 'POST':
                            alert('Saving error. Is system in database? Try to get and update.');
                            break;
                        default:
                            alert('Problem occured at operation');
                    }
                    console.log(xhr.statusText);
                };
            };

            xhr.send(data === null ? data : JSON.stringify(data));
        });
    };

    // Gets place from DB
    get_data() {
        if (! this.empty_title()) {
            let url = this.API_PREFIX + '?title=' + this.get_inputs().title;

            this.xhr('GET', url, '200', null).then(
                () => {
                    // Negative pk is forced and is sign of the issue
                    if (this.pk > 0) {
                        this.set_inputs(this.data[0]);
                        this.update_position(this.data[0]);
                    }
                },
                () => {
                    // simplest here
                    console.log('rejected');
                }
            );
        };
    };

    // Posts to DB
    set_data() {
        if (! this.empty_title()) {
            let url = this.API_PREFIX;
            this.xhr('POST', url, '201', this.get_inputs());
        };
    };

    // Deletes from DB
    delete_data() {
        if (! this.empty_title()) {
            let url = this.API_PREFIX + '?title=' + this.get_inputs().title;

            this.xhr('GET', url, '200', null).then(
                () => {
                    let url = this.API_PREFIX + this.pk + '/';
                    this.set_inputs('clear');
                    this.xhr('DELETE', url, '204', null);
                },
                () => {
                    // simplest here
                    console.log('rejected');
                }
            );
        };
    };

    // GETS from DB
    update_data() {
        if (! this.empty_title()) {
            let url = this.API_PREFIX + '?title=' + this.get_inputs().title;

            this.xhr('GET', url, '200', null).then(
                () => {
                    let url = this.API_PREFIX + this.pk + '/';
                    this.xhr('PUT', url, '200', this.get_inputs());
                },
                () => {
                    // simplest here
                    console.log('rejected');
                }
            );
        };
    };

    // Gets all places
    get_all_places(flag) {
        let url = this.API_PREFIX;

        if (flag !== 'keywords' || flag === 'keywords' && document.getElementById('keywords').value !== '') {
            this.xhr('GET', url, '200', null).then(
                () => {
                    this.generate_markers(this.data, flag);
                },
                () => {
                    // simplest here
                    console.log('rejected');
                }
            ) ;
        } else {
            alert('Please define keyword(s)');
        };
    };

    // ===========
    // GOOGLE MAPS
    // ===========

    // Google Map options. In real app I'd place it in a separate file or on top
    map_options() {
        return {
            zoom: 8,
            center: {
                lat: this.user_lat || 60,
                lng: this.user_lng || 25,
            },
        }
    };

    // Google Maps initialization
    init_map() {
        // Try HTML5 Geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                this.user_lat = position.coords.latitude;
                this.user_lng = position.coords.longitude;
                this.get_map();
            }, () => {
                alert('Geolocation is not allowed. Using basic coordinates.');
                this.get_map();
            },);
        } else {
            // Browser doesn't support Geolocation
            alert('Your browser does not support Geolocation.');
            this.get_map();
        };
    };

    get_map() {
        this.map = new google.maps.Map(document.getElementById('map'), this.map_options());
        this.coords_on_click();
    };

    update_position(place) {

        let lat_lng = new google.maps.LatLng(place.latitude, place.longitude);

        this.delete_markers(this.markers);

        let marker = new google.maps.Marker({
            position: lat_lng,
            map: this.map,
            animation: google.maps.Animation.EASE
        });

        marker.setPosition(lat_lng);
        this.map.setCenter(lat_lng);
        this.create_infowindow(marker, place.title);

        // Needed for marker deletion
        this.markers.push(marker);
    };

    // Getting opening and closing hours for maps
    filter_open(opening, closing) {

        let now = new Date().getTime(),
            ts = t => new Date().setHours(t.split(':')[0], t.split(':')[1], 0, 0);

        // Getting opening and closing hours for today
        opening = ts(opening);
        closing = ts(closing);

        // Some places are open night time
        if (opening >= closing) {
            let day = 24 * 60 * 60 * 1e3;
            now > closing ? closing += day : opening -= day;
        };

        // Returning open/closed state of the place
        return (now > Math.min(opening, closing) && now < Math.max(opening, closing)) ? true : false;
    };

    // Filter keywords
    filter_keywords(data, filter_str) {
        let regex = /[\s(,;./)]+/,
            keywords = data.keywords.split(regex),
            filter = filter_str.split(regex),
            match = 0;

        if (filter.length <= keywords.length) {
            for (let i = 0; i < filter.length; i++) {
                if (keywords.indexOf(filter[i]) >= 0) {
                    ++match;
                };
            };
        }

        return match === filter.length ? true : false;
    };

    // Takes care of markers on map.
    generate_markers(data, flag) {
        let condition;

        this.map_bounds = new google.maps.LatLngBounds();
        this.delete_markers(this.markers);

        for (let place in data) {
            switch (flag) {
                case 'open':
                    condition = this.filter_open(data[place].opening_hour, data[place].closing_hour);
                    break;
                case 'keywords':
                    condition = this.filter_keywords(data[place], document.getElementById('keywords').value);
                    break;
            };

            if (condition) {
                let lat_lng = new google.maps.LatLng(data[place].latitude, data[place].longitude),
                    marker = new google.maps.Marker({
                        position: lat_lng,
                        map: this.map,
                    });

                this.map_bounds.extend(lat_lng);
                marker.setPosition(lat_lng);
                this.create_infowindow(marker, data[place].title);

                // Needed for markers deletion
                this.markers.push(marker);
            };
        };

        this.map.fitBounds(this.map_bounds);
        this.map.panToBounds(this.map_bounds);
    };

    // Creating tooltip
    create_infowindow(marker, message) {
        let infowindow = new google.maps.InfoWindow({
            content: '<h5>' + message + '</h5>'
        });

        google.maps.event.addListener(marker, 'click', () => {
            infowindow.open(this.map, marker);
        });

        infowindow.open(this.map, marker);
    }

    delete_markers(markers) {
        // Loop through all the markers and remove
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    };

    coords_on_click() {
        let self = this;

        google.maps.event.addListener(this.map, 'click', function (e) {
            self.set_inputs('clear', () => {
                document.getElementById('latitude').value = e.latLng.lat().toFixed(6);
                document.getElementById('longitude').value = e.latLng.lng().toFixed(6);
            });
        });
    }
};

var mpe_app = new MpeApp();