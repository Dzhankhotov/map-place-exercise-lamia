from django.apps import AppConfig


class MpeAppConfig(AppConfig):
    name = 'mpe_app'
