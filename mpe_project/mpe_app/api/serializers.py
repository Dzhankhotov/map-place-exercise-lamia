from rest_framework import serializers
from mpe_app.models import Place

class PlacesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = [
            'pk',
            'title',
            'description',
            'latitude',
            'longitude',
            'opening_hour',
            'closing_hour',
            'keywords',
            'associated_places',
        ]
        read_only_fields = [
            'pk',
        ]

    def validate_title(self, value):
        qs = Place.objects.filter(title__iexact=value)

        # We only want to update and create titles
        if qs.exists() and self.context['request'].method != 'PUT':
            raise serializers.ValidationError("The title has already been used")

        return value