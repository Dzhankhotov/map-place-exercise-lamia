# Generic
from django.db.models import Q
from rest_framework import generics, mixins
from mpe_app.models import Place
from .serializers import PlacesSerializer
from datetime import datetime

class PlacesAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = 'pk'
    serializer_class = PlacesSerializer

    def get_queryset(self):
        qs = Place.objects.all()
        query_exact = self.request.GET.get('title')

        if query_exact is not None:
            # *__iexact and *__icontains provide case-sensitive match
            qs = qs.filter(Q(title__iexact=query_exact)|Q(keywords__iexact=query_exact)).distinct()

        return qs

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# Pass RetrieveAPIView, RetrieveUpdateAPIView  to remove some options
class PlacesRudView(generics.RetrieveUpdateDestroyAPIView):
    lookup_field = 'pk'
    serializer_class = PlacesSerializer

    def get_queryset(self):
        return Place.objects.all()
