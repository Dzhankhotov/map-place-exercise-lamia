from django.conf.urls import url
from .views import PlacesRudView, PlacesAPIView

urlpatterns = [
    url(r'^$', PlacesAPIView.as_view(), name='place-list'),
    url(r'^(?P<pk>\d+)/$', PlacesRudView.as_view(), name='place-rud'),
]