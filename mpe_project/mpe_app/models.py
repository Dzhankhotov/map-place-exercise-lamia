from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# This class describes places
class Place(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    latitude = models.TextField(blank=True)
    longitude = models.TextField(blank=True)
    opening_hour = models.TextField(blank=True)
    closing_hour = models.TextField(blank=True)
    keywords = models.CharField(max_length=250, blank=True)
    associated_places = models.CharField(max_length=250, blank=True)

    def __str__(self):
        return str(self.title)