import time, tempfile
import re
from django.shortcuts import render
from django.http import HttpResponse
from .models import Place

def get_published_objects():
    '''Updates objects, e.g. when toggling draft/published states'''

    return {
        'place': Place.objects.order_by('pk'),
    }

def map_place(request):
    '''This function is passed to the url of the configuration page
    It renders items available in database for selection ('published')'''

    return render(request, 'app.html', get_published_objects())

# Rendering error pages
# - Bad request
def error400(request):

    error = {
        'code': '400',
        'message': 'Bad request'
    }

    return render(request,'error_page.html', error)

# - Permission denied
def error403(request):

    error = {
        'code': '403',
        'message': 'Permission denied'
    }

    return render(request,'error_page.html', error)

# - Page not found
def error404(request):

    error = {
        'code': '404',
        'message': 'Page not found'
    }

    return render(request,'error_page.html', error)

# - Server error
def error500(request):

    error = {
        'code': '500',
        'message': 'Server error'
    }

    return render(request,'error_page.html', error)
