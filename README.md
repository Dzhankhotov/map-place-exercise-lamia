# MAP REPLACE EXERCISE FOR LAMIA
#### by Dr. Valentin Dzhankhotov,

[vushe.github.io](https://vushe.github.io)

dzhankhotov@gmail.com

tel. 050 464 1690

## External link
Try [example application](http://vushe.pythonanywhere.com/).

## Requirements
* Python [Django](https://www.djangoproject.com/). Django's REST framework is used for API. List of requirements can be found in [requirements.txt](requirements.txt).
* Docker and docker-compose. As mentioned in [docker-compose](docker-compose.yml), SASS and Livereload containers are optional.
* Database: MySQL. Django provides powerful middleware to interact with database using models in Python.
* HTML, JavaScript and SCSS/CSS code are placed in [templates](mpe_project/mpe_app/templates/) and [static](mpe_project/mpe_app/static/).
* Optimized for Ubuntu 16.04 compatible distros.

## Quick start
* clone project
* on Ubuntu, execute [./run.bash](run.bash), which shall automatically provide required jobs.
* to access tool via browser type in omnibar `localhost:8000`. Alterntively, use `docker-compose build` and use [./run.bash](run.bash) as reference.
* If docker error observed, make sure user is in docker group. If not, add user to this group and, in Ubuntu-based distros, log out then log in to apply updated group policies.

## Functionality
* Enter information about new place in form and press `Save` button to save it in database.
* Fill `Place` and search for it using button on the right side of the field. If item is in database, information about it will be loaded in form and its location shown on map.
* Use `Keywords` field (separated by comma) and its button to search in the database for a suitable places and to display them on map.
* When location is loaded from database using `Title` search button, `Update` button will update saved form.
* When location is loaded from database using `Title` search button, `Destroy` button deletes it database
* `Now open` will filter Places in database, which are open right now and display them on map.

## Notes
* API is located at [/api/places](http://vushe.pythonanywhere.com/api/places/)
* Use parameter `place` to query place by its title, e.g. `/api/place/?title=Helsinki`
* Please remember, that's just a simple app made in hurry in vanilla JS/SCSS ;-)